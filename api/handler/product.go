package handler

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com//e_commerce_api_gateway/genproto/product_service"
	product_service "gitlab.com/e_commerce_microservice/e_commerce_product_service/genproto"
)

// CreateProduct godoc
// @Router       /v1/products [post]
// @Summary      Create a new product
// @Description  Create a new product with the provided details
// @Tags         Products
// @Accept       json
// @Produce      json
// @Param        product     body  product_service.CreateProduct  true  "data of the product"
// @Success      201  {object}  product_service.CreateProductResp
// @Failure      400  {object}  Response{data=string}
// @Failure      404  {object}  Response{data=string}
// @Failure      500  {object}  Response{data=string}
func (h *Handler) CreateProduct(ctx *gin.Context) {
	var product = product_service.CreateProduct{}

	err := ctx.ShouldBindJSON(&product)
	if err != nil {
		h.handlerResponse(ctx, "CreateProduct", http.StatusBadRequest, err.Error())
		return
	}

	resp, err := h.services.ProductService().Create(ctx, &product_service.CreateProduct{
		Name:         product.Name,
		Price:        product.Price,
		Image:        product.Image,
		DiscountType: product.DiscountType,
		Discount:     product.Discount,
		Barcode:      product.Barcode,
	})

	if err != nil {
		h.handlerResponse(ctx, "ProductService().Create", http.StatusBadRequest, err.Error())

		return
	}

	h.handlerResponse(ctx, "create product response", http.StatusOK, resp)
}

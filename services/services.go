package services

import (
	product_service "gitlab.com/market_mc/market_go_product_service/genproto"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
	"honnef.co/go/tools/config"
)

type ServiceManagerI interface {
	// Product Service
	ProductService() product_service.ProductServiceClient
}

type grpcClients struct {
	// Product Service
	productService product_service.ProductServiceClient
}

func NewGrpcClients(cfg config.Config) (ServiceManagerI, error) {
	// Product Microservice
	connProductService, err := grpc.Dial(
		cfg.ProductServiceHost+cfg.ProductGRPCPort,
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	return &grpcClients{
		// Product Service
		productService: product_service.NewProductServiceClient(connProductService),
	}, nil
}

// Product Service
func (g *grpcClients) ProductService() product_service.ProductServiceClient {
	return g.productService
}
